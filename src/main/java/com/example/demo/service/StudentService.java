package com.example.demo.service;

import com.example.demo.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;
import java.util.Optional;

/**
 * Created by User on 05-Apr-21.
 */
public interface StudentService {


    Student create(Student student);

    Student update(Student student);

    Optional<Student> findById(String id);

    Page<Student> findAll(Pageable pageable);

    void delete(String id);

}
