package com.example.demo.entity;

import lombok.*;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Teacher {

    private Integer id;

    private String name;

    private String degination;


}
