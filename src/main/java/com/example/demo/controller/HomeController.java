package com.example.demo.controller;

import com.example.demo.entity.Student;
import com.example.demo.service.impl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/private")
public class HomeController {

    @Autowired
    StudentServiceImpl studentService;

    @GetMapping("/home")
    public String goHome(){

        return "Welcome to home page";
    }

    @PostMapping
    public ResponseEntity<Student> createStudent(@RequestBody Student student){

        student.setCreatedDate(new Date());
        student.setUpdatedDate(new Date());
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(studentService.create(student));
    }

    @PutMapping
    public ResponseEntity<Student> updateStudent(@RequestBody Student student){

        student.setUpdatedDate(new Date());
        return ResponseEntity.status(HttpStatus.ACCEPTED)
            .body(studentService.update(student));
    }

    @GetMapping
    public ResponseEntity<Page<Student>> getAllstudents(Pageable pageable){
        return ResponseEntity.ok(studentService.findAll(pageable));
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Optional<Student>> getStudentById(@PathVariable String id){
//        return ResponseEntitystudentService.findById(id);

        return ResponseEntity.ok(studentService.findById(id));

    }

    @DeleteMapping("/id/{id}")
    public ResponseEntity<Void> deleteStudent(@PathVariable String id){
        studentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }



}
