package com.example.demo.controller;

import com.example.demo.entity.Teacher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by User on 07-Apr-21.
 */
@RestController
public class TeacherController {

    @GetMapping("/teacher")
    public void testTeacher(){

        Teacher teacher = new Teacher();
        teacher.setId(123);
        teacher.setName("Md. Abul Bashar");
        teacher.setDegination("Lecturer");

        System.out.println("Teacher's Id : "+teacher.getId());
        System.out.println("Teacher's name : "+teacher.getName());
        System.out.println("Teacher's Designation : "+teacher.getDegination());

        Teacher teacher1 = new Teacher(1234,"Abdul","Professor");
        System.out.println(teacher1.toString());
    }
}
