package com.example.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;


@Setter
@Getter
@AllArgsConstructor
public class EntityNotFoundException extends RuntimeException {

    private String message;
}
